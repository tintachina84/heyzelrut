package com.heyzelrut.app.heyzelrut;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

public class ListenerService extends WearableListenerService {
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String[] messages = (messageEvent.getPath()).split(",");
        Log.e("MESSAGE ::: ", messageEvent.getPath());
        Log.e("MESSAGE ::: ", messages.toString());
        String lat = messages[0];
        String lon = messages[1];
        String eventName = messages[2];
        String eventContent = messages[3];

        int notificationId = 1;
        Intent mapIntent = new Intent(getApplicationContext(), MapsActivity.class);
        mapIntent.putExtra("lat", lat);
        mapIntent.putExtra("lon", lon);
        PendingIntent mapPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, mapIntent, 0);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.wear_ic_noti1)
                .setContentTitle(eventName)
                .setContentText(eventContent)
                .setContentIntent(mapPendingIntent);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
        notificationManager.notify(notificationId, notificationBuilder.build());

    }
}
