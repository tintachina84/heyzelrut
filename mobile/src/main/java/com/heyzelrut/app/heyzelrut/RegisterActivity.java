package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.heyzelrut.app.info.LoginInfo;
import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends Activity {
    private long backKeyPressedTime = 0;
    private Button registerBtn;
    private EditText registerName, registerId, registerPwd, registerBirth, registerPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerId = (EditText) findViewById(R.id.registerId);
        registerPwd = (EditText) findViewById(R.id.registerPwd);
        registerBirth = (EditText) findViewById(R.id.registerBirth);
        registerPhone = (EditText) findViewById(R.id.registerPhone);
        registerName = (EditText) findViewById(R.id.registerName);
        registerBtn = (Button) findViewById(R.id.registerBtn);

        /*진동울리기 시스템 서비스 가져오기*/
        /*Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(2000);*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void registerOk(View view) {
        //가입 버튼 클릭
        RegisterAsyncTask registerAsyncTask = new RegisterAsyncTask();
        registerAsyncTask.execute(registerId.getText().toString(), registerPwd.getText().toString(),
                registerBirth.getText().toString(), registerPhone.getText().toString(), registerName.getText().toString());
    }

    class RegisterAsyncTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String insertId = params[0];
            String insertPwd = params[1];
            String insertBirth = params[2];
            String insertPhone = params[3];
            String insertName = params[4];
            Map<String, String> registerParams = new HashMap<String, String>();
            registerParams.put("memberId",insertId);
            registerParams.put("memberPwd",insertPwd);
            registerParams.put("memberBirth",insertBirth);
            registerParams.put("memberPhone",insertPhone);
            registerParams.put("memberName",insertName);
            StringBuilder postMemberData = new StringBuilder();
            for(Map.Entry<String, String> param : registerParams.entrySet()){
                if(postMemberData.length() != 0) postMemberData.append("&");
                try {
                    postMemberData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postMemberData.append("=");
                    postMemberData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
                }catch (UnsupportedEncodingException e){
                    e.printStackTrace();
                }
            }
            byte postMemberDataBytes[] = new byte[0];
            try {
                postMemberDataBytes = postMemberData.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            HttpURLConnection connection = null;
            URL url = null;
            try {
                url = new URL(StaticInfo.IP_ADDR +"/insertMember");
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", String.valueOf(postMemberData.length()));

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(postMemberDataBytes);
                outputStream.flush();
                outputStream.close();

                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String result ="";
                while ((line=bufferedReader.readLine()) != null){
                    result+=line;
                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String memberJson) {
            super.onPostExecute(memberJson);
            String memberId ="";
            String memberPwd ="";
            String memberNo ="";
            String memberName ="";
            if(memberJson == null || memberJson.equals("")){
                Toast.makeText(getApplicationContext(), "회원가입 실패", Toast.LENGTH_SHORT).show();
            }else{
                try {
                    JSONObject jsonObject = new JSONObject(memberJson);
                    memberId = jsonObject.getString("memberId");
                    memberPwd = jsonObject.getString("memberPwd");
                    memberNo = jsonObject.getString("memberNo");
                    memberName = jsonObject.getString("memberName");
                    LoginInfo.LOGIN_MEMBER_NO = memberNo;
                    LoginInfo.LOGIN_MEMBER_ID = memberId;
                    LoginInfo.LOGIN_MEMBER_NAME = memberName;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(getApplicationContext(), Favor1Activity.class));
                finish();
            }
        }
    }

    /*뒤로가기 버튼 두번 클릭시 종료 시키기*/
    public void onBackPressed(){
        Toast toast;
        toast = Toast.makeText(this, "뒤로가기 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT);

        if(System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            toast.show();
            return;
        }else if(System.currentTimeMillis() <= backKeyPressedTime+ 2000){
            toast.cancel();
            finish();
            System.exit(0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.RegisterView));
        System.gc();
    }


    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

}
