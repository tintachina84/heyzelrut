package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.heyzelrut.app.info.FavorInfo;
import com.heyzelrut.app.info.LoginInfo;
import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Favor3Activity extends Activity {

    CheckBox checkBox1, checkBox2, checkBox3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favor3);
        checkBox1 = (CheckBox) findViewById(R.id.checkBox1);
        checkBox2 = (CheckBox) findViewById(R.id.checkBox2);
        checkBox3 = (CheckBox) findViewById(R.id.checkBox3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favor3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void nextClick3(View view) {
        FavorInfo.Favor_EventType_NO = new ArrayList<>();

        if(checkBox1.isChecked()==true){
            FavorInfo.Favor_EventType_NO .add("10");
        }
        if(checkBox2.isChecked()==true){
            FavorInfo.Favor_EventType_NO.add("20");
        }
        if(checkBox3.isChecked()==true){
            FavorInfo.Favor_EventType_NO.add("30");
        }

        /*Favor_Brand_NO ArrayList 불러오기*/
        String tempBrandNo = "";
        for(int i=0; i<FavorInfo.Favor_Brand_NO.size(); i++ ){
            tempBrandNo += FavorInfo.Favor_Brand_NO.get(i);
            tempBrandNo += ",";
        }
        String favorBrandNo = tempBrandNo.substring(0, tempBrandNo.length() -1);

        /*Favor_GoodsType_NO ArrayList 불러오기*/
        String tempGoodsTypeNo = "";
        for(int i=0; i<FavorInfo.Favor_GoodsType_NO.size(); i++){
            tempGoodsTypeNo += FavorInfo.Favor_GoodsType_NO.get(i);
            tempGoodsTypeNo +=",";
        }
        String favorGoodsTypeNo = tempGoodsTypeNo.substring(0, tempGoodsTypeNo.length() -1);

        /*Favor_EventType_NO*/
        String tempEventTypeNo = "";
        for(int i=0; i<FavorInfo.Favor_EventType_NO.size(); i++){
            tempEventTypeNo += FavorInfo.Favor_EventType_NO.get(i);
            tempEventTypeNo += ",";
        }
        String favorEventTypeNo = tempEventTypeNo.substring(0, tempEventTypeNo.length() -1);

/*        textView2.setText("favorBrandNo :" + favorBrandNo +":::"+ "favorGoodsTypeNo : " + favorGoodsTypeNo+":::" + "favorEventTypeNo : " + favorEventTypeNo);*/


        InterestAsyncTask interestEntry = new InterestAsyncTask();
        interestEntry.execute(LoginInfo.LOGIN_MEMBER_NO,favorBrandNo, favorGoodsTypeNo, favorEventTypeNo);

/*        Toast.makeText(getApplicationContext(), LoginInfo.LOGIN_MEMBER_NO+":::::"+FavorInfo.Favor_Brand_NO+"::::"+FavorInfo.Favor_GoodsType_NO+"::::::"
                +FavorInfo.Favor_EventType_NO, Toast.LENGTH_SHORT).show();*/
    }

    class InterestAsyncTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String memberNo = params[0];
            String brandNo = params[1];
            String goodsTypeNo = params[2];
            String eventTypeNo = params[3];
            Map<String, String> interestParams = new HashMap<String, String>();
            interestParams.put("memberNo", memberNo);
            interestParams.put("brandNo", brandNo);
            interestParams.put("goodsTypeNo", goodsTypeNo);
            interestParams.put("eventTypeNo", eventTypeNo);
            StringBuilder postInterestData = new StringBuilder();
            for(Map.Entry<String, String> param : interestParams.entrySet()){
                if(postInterestData.length() != 0) postInterestData.append("&");
                try {
                    postInterestData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postInterestData.append("=");
                    postInterestData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            byte postInterestDataBytes[] = new byte[0];
            try {
                postInterestDataBytes = postInterestData.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            HttpURLConnection connection = null;
            URL url = null;
            try {
                url = new URL(StaticInfo.IP_ADDR +"/interestList");
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", String.valueOf(postInterestData.length()));

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(postInterestDataBytes);
                outputStream.flush();
                outputStream.close();

                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line="";
                String result="";
                while((line=bufferedReader.readLine()) != null){
                    result+=line;
                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String interestJson) {
            super.onPostExecute(interestJson);
            if(interestJson == null || interestJson.equals("")){
                Toast.makeText(getApplicationContext(), "관심사 등록 실패", Toast.LENGTH_SHORT).show();
            }
            /*다음에 이동할 페이지 수정*/
            Toast.makeText(getApplicationContext(),"관심사 등록 성공", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), TabMenuActivity.class));
            finish();
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.Favor3View));
        System.gc();
    }


    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

}
