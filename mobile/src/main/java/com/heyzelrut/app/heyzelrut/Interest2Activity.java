package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.heyzelrut.app.info.InterestInfo;
import com.heyzelrut.app.info.LoginInfo;
import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Interest2Activity extends ActivityGroup {
    CheckBox goodsCheck1,goodsCheck2,goodsCheck3,goodsCheck4,goodsCheck5,goodsCheck6,goodsCheck7;
    Button interestBtn2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest2);
        goodsCheck1 = (CheckBox) findViewById(R.id.goodsCheck1);
        goodsCheck2 = (CheckBox) findViewById(R.id.goodsCheck2);
        goodsCheck3 = (CheckBox) findViewById(R.id.goodsCheck3);
        goodsCheck4 = (CheckBox) findViewById(R.id.goodsCheck4);
        goodsCheck5 = (CheckBox) findViewById(R.id.goodsCheck5);
        goodsCheck6 = (CheckBox) findViewById(R.id.goodsCheck6);
        goodsCheck7 = (CheckBox) findViewById(R.id.goodsCheck7);
        interestBtn2 = (Button) findViewById(R.id.interestBtn2);

        InterestGoodsAsyncTask interestGoodsAsyncTask = new InterestGoodsAsyncTask();
        interestGoodsAsyncTask.execute(LoginInfo.LOGIN_MEMBER_NO);
    }

    class InterestGoodsAsyncTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String memberNo = params[0];
            Map<String, String> interestParams = new HashMap<String, String>();
            interestParams.put("memberNo", memberNo);
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, String> param : interestParams.entrySet()) {
                if (postData.length() != 0) postData.append("&");
                try {
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append("=");
                    postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            byte postDataBytes[] = new byte[0];
            try {
                postDataBytes = postData.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            HttpURLConnection connection = null;
            try {
                URL url = new URL(StaticInfo.IP_ADDR +"/interestGoods");

                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(postDataBytes);
                outputStream.flush();
                outputStream.close();

                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String result = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            ArrayList<String> goodsTypeNoList = new ArrayList<>();
            if(json == null || json.equals("")){
                Toast.makeText(getApplicationContext(), "정보 불러오기 실패", Toast.LENGTH_SHORT).show();
            }else{
                try {
                    JSONArray jsonArray = new JSONArray(json);
                    for(int i=0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        goodsTypeNoList.add(jsonObject.getString("goodsTypeNo"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
/*          String tempList = "";*/
            for(int i=0; i<goodsTypeNoList.size();i++){
/*              tempList += goodsTypeNoList.get(i);
                tempList += ",";*/
                switch(goodsTypeNoList.get(i)){
                    case "10" : goodsCheck1.setChecked(true); break;
                    case "20" : goodsCheck2.setChecked(true); break;
                    case "30" : goodsCheck3.setChecked(true); break;
                    case "40" : goodsCheck4.setChecked(true); break;
                    case "50" : goodsCheck5.setChecked(true); break;
                    case "60" : goodsCheck6.setChecked(true); break;
                    case "70" : goodsCheck7.setChecked(true); break;
                }
            }
/*            Toast.makeText(getApplicationContext(), "::::"+tempList, Toast.LENGTH_SHORT).show();*/
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_interest2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void interestClick2(View view) {
        InterestInfo.Interest_GoodsType_NO = new ArrayList<>();

        if(goodsCheck1.isChecked()==true){
            InterestInfo.Interest_GoodsType_NO.add("10");
        }
        if(goodsCheck2.isChecked()==true){
            InterestInfo.Interest_GoodsType_NO.add("20");
        }
        if(goodsCheck3.isChecked()==true){
            InterestInfo.Interest_GoodsType_NO.add("30");
        }
        if(goodsCheck4.isChecked()==true){
            InterestInfo.Interest_GoodsType_NO.add("40");
        }
        if(goodsCheck5.isChecked()==true){
            InterestInfo.Interest_GoodsType_NO.add("50");
        }
        if(goodsCheck6.isChecked()==true){
            InterestInfo.Interest_GoodsType_NO.add("60");
        }
        if(goodsCheck7.isChecked()==true){
            InterestInfo.Interest_GoodsType_NO.add("70");
        }
        Intent intent = new Intent(Interest2Activity.this, Interest3Activity.class);
        Window window = getLocalActivityManager().startActivity("Interest2", intent);
        setContentView(window.getDecorView());

        //startActivity(new Intent(getApplicationContext(), Interest3Activity.class));
        //finish();
    }


    /*@Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.Interest2View));
        System.gc();
    }


    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }*/


}
