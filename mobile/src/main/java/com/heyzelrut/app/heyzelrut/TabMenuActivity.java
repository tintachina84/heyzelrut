package com.heyzelrut.app.heyzelrut;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

import com.heyzelrut.app.service.GpsService;


public class TabMenuActivity extends TabActivity {

    private long backKeyPressedTime = 0;
    Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_menu);

        Resources res = this.getResources();
        TabHost tabHost = this.getTabHost();
        TabHost.TabSpec spec;
        Intent intent = null;

        /*intent = new Intent().setClass(this, MenuActivity.class);
        spec = tabHost.newTabSpec("MenuActivity").setIndicator("실험용메뉴", res.getDrawable(R.drawable.mainsplash)).setContent(intent);
        tabHost.addTab(spec);*/

        intent = new Intent().setClass(this, EventActivity.class);
        spec = tabHost.newTabSpec("EventActivity").setIndicator("이벤트").setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, Interest1Activity.class);
        spec = tabHost.newTabSpec("Interest1Activity").setIndicator("관심상품").setContent(intent);
        tabHost.addTab(spec);

        tabHost.setCurrentTab(0);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tab_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (serviceIntent == null) {
            serviceIntent = new Intent(getApplicationContext(), GpsService.class);
            startService(serviceIntent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*Intent gpsServiceIntent = new Intent(getApplicationContext(), GpsService.class);
        stopService(gpsServiceIntent);
        finish();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Toast toast;
        toast = Toast.makeText(this, "뒤로가기 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT);

        if(System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            toast.show();
            return;
        }else if(System.currentTimeMillis() <= backKeyPressedTime+ 2000){
            toast.cancel();
            finish();
            System.exit(0);
        }
    }
}