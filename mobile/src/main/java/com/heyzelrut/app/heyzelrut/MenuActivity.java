package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.heyzelrut.app.info.GpsInfo;
import com.heyzelrut.app.service.GpsService;

public class MenuActivity extends Activity {
    private long backKeyPressedTime = 0;
    private TextView gpsTextView;
    private Button gpsBtn;

    private double latitude;
    private double longitude;

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        gpsTextView = (TextView) findViewById(R.id.gpsTextView);
        gpsBtn = (Button) findViewById(R.id.gpsBtn);

        gpsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gpsTextView.setText("위도 : " + GpsInfo.LATITUDE + ", 경도 : " + GpsInfo.LONGITUDE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent serviceIntent = new Intent(getApplicationContext(), GpsService.class);
        startService(serviceIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent gpsServiceIntent = new Intent(getApplicationContext(), GpsService.class);
        stopService(gpsServiceIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Toast toast;
        toast = Toast.makeText(this, "뒤로가기 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT);

        if(System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            toast.show();
            return;
        }else if(System.currentTimeMillis() <= backKeyPressedTime+ 2000){
            toast.cancel();
            finish();
            System.exit(0);
        }
    }
}
