package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.heyzelrut.app.info.InterestInfo;
import com.heyzelrut.app.info.LoginInfo;
import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Interest1Activity extends ActivityGroup {
    CheckBox nike,adidas,newBalance,macdonalds,burgerking,kfc,lotteria,innisfree,missha,aritaum;
    Button interestBtn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest1);

        nike = (CheckBox) findViewById(R.id.nike);
        adidas = (CheckBox) findViewById(R.id.adidas);
        newBalance = (CheckBox) findViewById(R.id.newBalance);
        macdonalds = (CheckBox) findViewById(R.id.macdonalds);
        burgerking = (CheckBox) findViewById(R.id.burgerking);
        kfc = (CheckBox) findViewById(R.id.kfc);
        lotteria = (CheckBox) findViewById(R.id.lotteria);
        innisfree = (CheckBox) findViewById(R.id.innisfree);
        missha = (CheckBox) findViewById(R.id.missha);
        aritaum = (CheckBox) findViewById(R.id.aritaum);
        interestBtn1 = (Button) findViewById(R.id.interestBtn1);

        InterestAsyncTask interestAsyncTask = new InterestAsyncTask();
        interestAsyncTask.execute(LoginInfo.LOGIN_MEMBER_NO);
    }
    /*관심 정보 불러오기*/
    class InterestAsyncTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String memberNo = params[0];
            Map<String, String> interestParams = new HashMap<String, String>();
            interestParams.put("memberNo", memberNo);
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, String> param : interestParams.entrySet()) {
                if (postData.length() != 0) postData.append("&");
                try {
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append("=");
                    postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            byte postDataBytes[] = new byte[0];
            try {
                postDataBytes = postData.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            HttpURLConnection connection = null;

            try {

                URL url = new URL(StaticInfo.IP_ADDR +"/interestBrand");

                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(postDataBytes);
                outputStream.flush();
                outputStream.close();

                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String result = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            ArrayList<String> brandNoList = new ArrayList<>();
            if (json == null || json.equals("")) {
                Toast.makeText(getApplicationContext(), "정보 불러오기 실패", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    /*brandNo Array로 받아서 배열에 저장*/
                    JSONArray jsonArray = new JSONArray(json);
                    for(int i=0; i<jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        brandNoList.add(jsonObject.getString("brandNo"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            /*체크박스 표시하기*/
            for(int i=0; i<brandNoList.size();i++){
                switch (brandNoList.get(i)){
                    case "10" : nike.setChecked(true); break;
                    case "20" : adidas.setChecked(true); break;
                    case "30" : newBalance.setChecked(true); break;
                    case "40" : macdonalds.setChecked(true); break;
                    case "50" : burgerking.setChecked(true); break;
                    case "60" : kfc.setChecked(true); break;
                    case "70" : lotteria.setChecked(true); break;
                    case "80" : innisfree.setChecked(true); break;
                    case "90" : missha.setChecked(true); break;
                    case "100" : aritaum.setChecked(true); break;
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_interest1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void interestClick1(View view) {
        InterestInfo.Interest_Brand_NO = new ArrayList<>();

        if(nike.isChecked()==true){
            InterestInfo.Interest_Brand_NO.add("10");
        }
        if(adidas.isChecked()==true){
            InterestInfo.Interest_Brand_NO.add("20");
        }
        if(newBalance.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("30");
        }
        if(macdonalds.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("40");
        }
        if(burgerking.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("50");
        }
        if(kfc.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("60");
        }
        if(lotteria.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("70");
        }
        if(innisfree.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("80");
        }
        if(missha.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("90");
        }
        if(aritaum.isChecked()==true) {
            InterestInfo.Interest_Brand_NO.add("100");
        }
        Intent intent = new Intent(Interest1Activity.this, Interest2Activity.class);
        Window window = getLocalActivityManager().startActivity("Interest2", intent);
        setContentView(window.getDecorView());
        //startActivity(new Intent(getApplicationContext(),Interest2Activity.class));
//        finish();
    }


    /*@Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.Interest1View));
        System.gc();
    }


    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }*/

}
