package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.UserHandle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.heyzelrut.app.adapter.EventAdapter;
import com.heyzelrut.app.info.EventEntity;
import com.heyzelrut.app.info.LoginInfo;
import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class EventActivity extends Activity {

    private ListView eventLists;
    ArrayList<EventEntity> eventList = new ArrayList<EventEntity>();

    static final String URLURL = StaticInfo.IP_ADDR + "/eventJSON?memberNo="+ LoginInfo.LOGIN_MEMBER_NO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        eventLists = (ListView)findViewById(R.id.eventLists);
        eventLists.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), position + "번째 항목", Toast.LENGTH_LONG).show();

                EventEntity entity = (EventEntity) parent.getItemAtPosition(position);
                String eventNo = entity.getEventNo()+"";
                Log.e("eventNo ::", "" + eventNo);
                Intent intent = new Intent(getApplicationContext(), EventViewActivity.class);
                intent.putExtra("eventNo", eventNo);
                startActivity(intent);

            }
        });
        getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        callEventAsyncTask callEventAsyncTask = new callEventAsyncTask();
        callEventAsyncTask.execute(URLURL);
    }

    Context context;
    LayoutInflater inflater;


    class callEventAsyncTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String result = "";

            try {
                URL url = new URL(params[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                InputStream is = conn.getInputStream();
                int size = 0;
                Log.e("size ::", size + "");
                byte[] buffer = new byte[512];
                while((size = is.read(buffer)) != -1) {
                    result = result + new String(buffer, 0, size);
                }
                is.close();

                /*JSONArray jarr = new JSONArray(result);
                int jsize = jarr.length();
                Log.e("jsize", jsize+"");
                for(int i=0; i<jsize; i++){
                    json = jarr.getJSONObject(i);
                    Log.e("JSON DATA", json.toString());
                    int evGNo = json.getInt("eventNo");
                    String evImg = json.getString("eventImgpath");
                    String evName = json.getString("eventName");
                    String evContent = json.getString("eventContent");
                    int evGoods = json.getInt("goodsNo");
                    String evGen = json.getString("eventGendate");
                    String evStart = json.getString("eventStdate");
                    String evEnd = json.getString("eventEddate");
                    int eSale = json.getInt("eventSale");
                    int eStat = json.getInt("eventStat");

                    int evECombo = json.getInt("eventCombo");
                    int evType = json.getInt("eventTypeNo");
                    int evGCombo = json.getInt("goodsCombo");

                    EventEntity entity = new EventEntity(evGNo, evName, evContent, evStart, evEnd,eSale, eStat, evImg, evGen, evGoods, evECombo,evType,evGCombo);
                    list.add(entity);
                }*/
            } catch (MalformedURLException e) {
                Log.e("DATA", "통신 에러");
                throw new RuntimeException(e.getMessage());
            } catch (IOException e) {
                Log.e("DATA", "입출력 에러");
                throw new RuntimeException(e.getMessage());
            }
            return result;
        }
        @Override
        protected void onPostExecute(String str){
            ArrayList<EventEntity> list = new ArrayList<EventEntity>();
            JSONObject json = new JSONObject();
            JSONArray jarr = null;
            try {
                jarr = new JSONArray(str);
                int jsize = jarr.length();
                Log.e("jsize", jsize+"");
                for(int i=0; i<jsize; i++){
                    json = jarr.getJSONObject(i);
                    Log.e("JSON DATA", json.toString());
                    int evGNo = json.getInt("eventNo");
                    String evImg = json.getString("eventImgpath");
                    String evName = json.getString("eventName");
                    String evContent = json.getString("eventContent");
                    int evGoods = json.getInt("goodsNo");
                    String evGen = json.getString("eventGendate");
                    String evStart = json.getString("eventStdate");
                    String evEnd = json.getString("eventEddate");
                    int eSale = json.getInt("eventSale");
                    int eStat = json.getInt("eventStat");

                    int evECombo = json.getInt("eventCombo");
                    int evType = json.getInt("eventTypeNo");
                    int evGCombo = json.getInt("goodsCombo");

                    EventEntity entity = new EventEntity(evGNo, evName, evContent, evStart, evEnd,eSale, eStat, evImg, evGen, evGoods, evECombo,evType,evGCombo);
                    list.add(entity);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("list?", list.toString());
            eventList = list;
            Log.e("context2", getApplicationContext().toString());
            Log.e("Inflate2", getLayoutInflater().toString());
            EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), eventList);
            eventLists.setAdapter(eventAdapter);
        }
    };

}
