package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class EventViewActivity extends Activity {

    private TextView textTitle;
    private TextView textContent;
    private String eventNo;
    private Button btnMap;
    private Button btnMenu;
    private long backKeyPressedTime = 0;

    private ImageView eveImg;

    private String lat;
    private String lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_view);
        getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        textTitle = (TextView) findViewById(R.id.textTitle);
        textContent = (TextView) findViewById(R.id.textContent);
        btnMap = (Button) findViewById(R.id.btnMap);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        eveImg = (ImageView) findViewById(R.id.eveImg);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            if (extras.containsKey("eventNo")) {
                eventNo = extras.getString("eventNo");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventDetailAsyncTask eventDetailAsyncTask = new EventDetailAsyncTask();
        eventDetailAsyncTask.execute(eventNo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onMapClick(View view) {
        Intent mapIntent = new Intent(getApplicationContext(), MapsActivity.class);
        mapIntent.putExtra("lat", lat);
        mapIntent.putExtra("lon", lon);
        startActivity(mapIntent);
        finish();
    }

    public void onMenuClick(View view) {
        startActivity(new Intent(getApplicationContext(), TabMenuActivity.class));
        finish();
    }

    class EventDetailAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;

            try {
                URL url = new URL(StaticInfo.IP_ADDR + "/getEventDetail?eventNo=" + params[0]);
                connection = (HttpURLConnection) url.openConnection();
                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String resultStr = "";
                while ((line = bufferedReader.readLine()) != null) {
                    resultStr += line;
                }

                return resultStr;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            try {
                JSONObject jsonObject = new JSONObject(json);

                String url = jsonObject.getString("eventImgpath");
                String path = StaticInfo.IP_ADDR+"/eventImgs/"+url;
                try {
                    InputStream is = new URL(path).openStream();
                    Bitmap bit = BitmapFactory.decodeStream(is);
                    eveImg.setImageBitmap(bit);
                    eveImg.setScaleType(ImageView.ScaleType.FIT_START);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                textTitle.setText(jsonObject.getString("eventName"));
                textContent.setText(jsonObject.getString("eventContent"));
                lat = jsonObject.getString("sellerLat");
                lon = jsonObject.getString("sellerLon");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    @Override
    public void onBackPressed() {
        Toast toast;
        toast = Toast.makeText(getApplicationContext(), "뒤로가기 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT);

        if(System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            toast.show();
            return;
        }else if(System.currentTimeMillis() <= backKeyPressedTime+ 2000){
            toast.cancel();
            finish();
            System.exit(0);
        }
    }
}
