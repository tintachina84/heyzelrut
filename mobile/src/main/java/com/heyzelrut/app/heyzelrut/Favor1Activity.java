package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.heyzelrut.app.info.FavorInfo;
import com.heyzelrut.app.info.LoginInfo;

import java.util.ArrayList;

public class Favor1Activity extends Activity {

    CheckBox nike, adidas, newBalance,macdonalds,burgerking,kfc,lotteria,innisfree,missha,aritaum;
    Button nextBtn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favor1);

/*        Toast.makeText(this, "memberNO"+ LoginInfo.LOGIN_MEMBER_NO, Toast.LENGTH_SHORT).show();*/

        nike = (CheckBox) findViewById(R.id.nike);
        adidas = (CheckBox) findViewById(R.id.adidas);
        newBalance = (CheckBox) findViewById(R.id.newBalance);
        macdonalds = (CheckBox) findViewById(R.id.macdonalds);
        burgerking = (CheckBox) findViewById(R.id.burgerking);
        kfc = (CheckBox) findViewById(R.id.kfc);
        lotteria = (CheckBox) findViewById(R.id.lotteria);
        innisfree = (CheckBox) findViewById(R.id.innisfree);
        missha = (CheckBox) findViewById(R.id.missha);
        aritaum = (CheckBox) findViewById(R.id.aritaum);
        nextBtn1 = (Button) findViewById(R.id.nextBtn1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favor1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void nextClick1(View view) {
        FavorInfo.Favor_Brand_NO = new ArrayList<>();

        if(nike.isChecked()==true){
            FavorInfo.Favor_Brand_NO.add("10");
        }
        if(adidas.isChecked()==true){
            FavorInfo.Favor_Brand_NO.add("20");
        }
        if(newBalance.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("30");
        }
        if(macdonalds.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("40");
        }
        if(burgerking.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("50");
        }
        if(kfc.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("60");
        }
        if(lotteria.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("70");
        }
        if(innisfree.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("80");
        }
        if(missha.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("90");
        }
        if(aritaum.isChecked()==true) {
            FavorInfo.Favor_Brand_NO.add("100");
        }

        startActivity(new Intent(getApplicationContext(), Favor2Activity.class));
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.Favor1View));
        System.gc();
    }


    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }
}