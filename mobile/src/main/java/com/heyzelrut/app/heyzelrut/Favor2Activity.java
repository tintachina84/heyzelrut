package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.heyzelrut.app.info.FavorInfo;

import java.util.ArrayList;

public class Favor2Activity extends Activity {

    CheckBox goodsCheck1,goodsCheck2,goodsCheck3,goodsCheck4,goodsCheck5,goodsCheck6,goodsCheck7;
    Button nextBtn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favor2);

        goodsCheck1 = (CheckBox) findViewById(R.id.goodsCheck1);
        goodsCheck2 = (CheckBox) findViewById(R.id.goodsCheck2);
        goodsCheck3 = (CheckBox) findViewById(R.id.goodsCheck3);
        goodsCheck4 = (CheckBox) findViewById(R.id.goodsCheck4);
        goodsCheck5 = (CheckBox) findViewById(R.id.goodsCheck5);
        goodsCheck6 = (CheckBox) findViewById(R.id.goodsCheck6);
        goodsCheck7 = (CheckBox) findViewById(R.id.goodsCheck7);
        nextBtn2 = (Button) findViewById(R.id.nextBtn2);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favor2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void nextClick2(View view) {
        FavorInfo.Favor_GoodsType_NO = new ArrayList<>();

        if(goodsCheck1.isChecked()==true){
            FavorInfo.Favor_GoodsType_NO.add("10");
        }
        if(goodsCheck2.isChecked()==true){
            FavorInfo.Favor_GoodsType_NO.add("20");
        }
        if(goodsCheck3.isChecked()==true){
            FavorInfo.Favor_GoodsType_NO.add("30");
        }
        if(goodsCheck4.isChecked()==true){
            FavorInfo.Favor_GoodsType_NO.add("40");
        }
        if(goodsCheck5.isChecked()==true){
            FavorInfo.Favor_GoodsType_NO.add("50");
        }
        if(goodsCheck6.isChecked()==true){
            FavorInfo.Favor_GoodsType_NO.add("60");
        }
        if(goodsCheck7.isChecked()==true){
            FavorInfo.Favor_GoodsType_NO.add("70");
        }
        startActivity(new Intent(getApplicationContext(), Favor3Activity.class));
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.Favor2View));
        System.gc();
    }


    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

}