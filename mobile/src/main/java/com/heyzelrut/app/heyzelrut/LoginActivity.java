package com.heyzelrut.app.heyzelrut;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.heyzelrut.app.info.LoginInfo;
import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity {

    private Button loginBtn;
    private Button joinBtn;
    private EditText inputIdTxt;
    private EditText inputPwdTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inputIdTxt = (EditText) findViewById(R.id.inputIdTxt);
        inputPwdTxt = (EditText) findViewById(R.id.inputPwdTxt);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        joinBtn = (Button) findViewById(R.id.joinBtn);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        unbindDrawables(findViewById(R.id.LoginView));
        System.gc();
    }


    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onLoginClick(View view) {
        LoginAsyncTask loginAsyncTask = new LoginAsyncTask();
        loginAsyncTask.execute(inputIdTxt.getText().toString(), inputPwdTxt.getText().toString());
    }

    public void onJoinClick(View view) {
        // 회원가입 화면 인텐트 호출
        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
        finish();
    }

    class LoginAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String inputId = params[0];
            String inputPwd = params[1];
            Map<String, String> loginParams = new HashMap<String, String>();
            loginParams.put("loginId", inputId);
            loginParams.put("loginPwd", inputPwd);
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, String> param : loginParams.entrySet()) {
                if (postData.length() != 0) postData.append("&");
                try {
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append("=");
                    postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            byte postDataBytes[] = new byte[0];
            try {
                postDataBytes = postData.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            HttpURLConnection connection = null;

            try {
                Log.e("IP ADDRESS ::: ", StaticInfo.IP_ADDR);
                URL url = new URL(StaticInfo.IP_ADDR + "/getLoginInfo");

                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(postDataBytes);
                outputStream.flush();
                outputStream.close();

                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String result = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            if(json == null || json.equals("")) {
                Toast.makeText(getApplicationContext(), "로그인 정보 획득 실패", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String memberId = jsonObject.getString("memberId");
                    String memberPwd = jsonObject.getString("memberPwd");
                    String memberNo = jsonObject.getString("memberNo");
                    String memberName = jsonObject.getString("memberName");
//                    Toast.makeText(getApplicationContext(), memberNo + " ::: " + memberId + " ::: " + memberPwd + " ::: " + memberName, Toast.LENGTH_SHORT).show();
                    LoginInfo.LOGIN_MEMBER_NO = memberNo;
                    LoginInfo.LOGIN_MEMBER_ID = memberId;
                    LoginInfo.LOGIN_MEMBER_NAME = memberName;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // 로그인 성공 후 다음 Intent 호출
                startActivity(new Intent(getApplicationContext(), TabMenuActivity.class));
                finish();
                // startActivity(new Intent(getApplicationContext(), Interest1Activity.class));
            }

        }
    }
}
