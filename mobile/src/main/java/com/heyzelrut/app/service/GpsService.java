package com.heyzelrut.app.service;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.heyzelrut.app.heyzelrut.EventViewActivity;
import com.heyzelrut.app.heyzelrut.R;
import com.heyzelrut.app.info.GpsInfo;
import com.heyzelrut.app.info.LoginInfo;
import com.heyzelrut.app.info.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class GpsService extends Service {

    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10;
    private int startId;
    private ArrayList<String> myEventList;


    private static long CONNECTION_TIME_OUT_MS = 10000;
    private static String MESSAGE = "TEST_MESSAGE";
    private String nodeId;


    class LocationListener implements android.location.LocationListener {

        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener");
            this.mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged ::: " + location);
            mLastLocation.set(location);
            GpsInfo.LATITUDE = mLastLocation.getLatitude();
            GpsInfo.LONGITUDE = mLastLocation.getLongitude();

            ListMemberEventAsyncTask listMemberEventAsyncTask = new ListMemberEventAsyncTask();
            listMemberEventAsyncTask.execute(LoginInfo.LOGIN_MEMBER_NO);

            GpsAsyncTask gpsAsyncTask = new GpsAsyncTask();
            gpsAsyncTask.execute(String.valueOf(GpsInfo.LATITUDE), String.valueOf(GpsInfo.LONGITUDE), LoginInfo.LOGIN_MEMBER_NO);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    android.location.LocationListener[] mLocationListenrs = new android.location.LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        this.startId = startId;
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();

        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, mLocationListenrs[1]);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, mLocationListenrs[0]);


        retrieveDeviceNode();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListenrs.length; i++) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mLocationManager.removeUpdates(mLocationListenrs[i]);
            }
        }
        super.onDestroy();
        stopSelf(startId);
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    class GpsAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String parameters = "latitude="+params[0]+"&longitude="+params[1]+"&memberNo="+LoginInfo.LOGIN_MEMBER_NO;

            HttpURLConnection connection = null;
            try {
                URL url = new URL(StaticInfo.IP_ADDR + "/getEventInfo?"+parameters);
                connection = (HttpURLConnection) url.openConnection();
                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String resultStr = "";
                while ((line = bufferedReader.readLine()) != null) {
                    resultStr += line;
                }

                return resultStr;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            Log.e(TAG, json);
            int notificationId = 0;
            try {
                JSONObject jsonObject = new JSONObject(json);
                JSONArray jsonArray = jsonObject.getJSONArray("eventList");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject obj = (JSONObject) jsonArray.get(i);

                    ++ notificationId;
                    Log.e("NOTIFICATION ID : ", String.valueOf(notificationId));

                    if (!myEventList.contains(obj.getString("eventNo"))) {


                        /*NotificationManagerCompat.from(getApplicationContext()).cancelAll();
                        Intent viewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                        PendingIntent viewPendingIntent = PendingIntent.getActivity(getApplicationContext(), notificationId, viewIntent, 0);
                        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
                        bigStyle.bigText("Wear 전용 텍스트가 보입니다.");
                        NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender()
                                .setHintHideIcon(true)
                                .setContentIcon(R.drawable.cast_ic_notification_0)
//                            .setBackground(BitmapFactory.decodeResource(getResources(), R.drawable.powered_by_google_light))
                                ;
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                                .addAction(R.drawable.cast_ic_notification_0, "확인", viewPendingIntent)
                                .setContentTitle(obj.getString("eventName"))
                                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.cast_ic_notification_on))
                                .setContentText(obj.getString("eventContent"))
                                .setSmallIcon(R.drawable.cast_ic_notification_2)
                                .setVibrate(new long[]{0, 1000})
                                .setStyle(bigStyle);
//                                .extend(wearableExtender);*/

                        Intent launchApp = new Intent(getApplicationContext(), EventViewActivity.class);
                        launchApp.putExtra("eventNo", obj.getString("eventNo"));
                        launchApp.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        launchApp.setAction("VIEW_DETAILS_PROPERTY");
                        PendingIntent launchNotification = PendingIntent.getActivity(getApplicationContext(), 0, launchApp, PendingIntent.FLAG_UPDATE_CURRENT);

                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.wear_ic_noti1)
                                .setContentTitle(obj.getString("eventName"))
                                .setContentText(obj.getString("eventContent"))
                                .setVibrate(new long[]{0, 1000, 500, 2000})
                                .setContentIntent(launchNotification);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
                        notificationManager.notify(notificationId, notificationBuilder.build());

                        /*Notification notification = builder.build();
                        notification.flags |= Notification.FLAG_AUTO_CANCEL;
                        notification.contentIntent = launchNotification;
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
                        notificationManager.notify(notificationId, notification);*/
                        Log.e("EVENT INFO :::::::::   ", obj.getString("eventName"));
                        Log.e("EVENT INFO :::::::::   ", obj.getString("eventContent"));
                    /*if (jsonArray.length() > 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }*/


                        // TO WEARABLE
                        String eventMessage = obj.getString("sellerLat")+","+obj.getString("sellerLon")+","
                                +obj.getString("eventName")+","+obj.getString("eventContent");
                        sendEventMessage(eventMessage);


                        CheckExistMemberEventAsyncTask checkExistMemberEventAsyncTask = new CheckExistMemberEventAsyncTask();
                        checkExistMemberEventAsyncTask.execute(String.valueOf(LoginInfo.LOGIN_MEMBER_NO), obj.getString("eventNo"));

                    /*MemberEventAsyncTask memberEventAsyncTask = new MemberEventAsyncTask();
                    memberEventAsyncTask.execute(String.valueOf(LoginInfo.LOGIN_MEMBER_NO), obj.getString("eventNo"));*/


                    }




                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    class MemberEventAsyncTask extends AsyncTask<String, Void, String> {

        String eventNo = "";

        @Override
        protected String doInBackground(String... params) {
            String memberNo = params[0];
            eventNo = params[1];

            Map<String, String> memberEventParams = new HashMap<String, String>();
            memberEventParams.put("memberNo", memberNo);
            memberEventParams.put("eventNo", eventNo);
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, String> param : memberEventParams.entrySet()) {
                if (postData.length() != 0) postData.append("&");
                try {
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append("=");
                    postData.append(URLEncoder.encode(param.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            byte postDataBytes[] = new byte[0];
            try {
                postDataBytes = postData.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Log.e("MEMBEREVENT ::: ", memberNo+", "+eventNo);

            HttpURLConnection connection = null;

            try {
                URL url = new URL(StaticInfo.IP_ADDR + "/setMemberEvent");

                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(postDataBytes);
                outputStream.flush();
                outputStream.close();

                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String result = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            Log.e("INSERT ::++ ", json);
        }
    }


    class CheckExistMemberEventAsyncTask extends AsyncTask<String, Void, String> {

        private String eventNo = "";

        @Override
        protected String doInBackground(String... params) {
            eventNo = params[1];
            String parameters = "memberNo="+params[0]+"&eventNo="+params[1];

            HttpURLConnection connection = null;
            try {
                URL url = new URL(StaticInfo.IP_ADDR + "/isExistMemberEvent?"+parameters);
                connection = (HttpURLConnection) url.openConnection();
                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String resultStr = "";
                while ((line = bufferedReader.readLine()) != null) {
                    resultStr += line;
                }

                return resultStr;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("MEMBER_EXIST : ", result);
            if(result.equals("false")) {
                MemberEventAsyncTask memberEventAsyncTask = new MemberEventAsyncTask();
                memberEventAsyncTask.execute(String.valueOf(LoginInfo.LOGIN_MEMBER_NO), eventNo);
            }
        }
    }

    class ListMemberEventAsyncTask extends AsyncTask<String, Void, String> {
        String memberNo = "";
        @Override
        protected String doInBackground(String... params) {
            memberNo = params[0];
            String parameters = "memberNo="+memberNo;

            HttpURLConnection connection = null;
            try {
                URL url = new URL(StaticInfo.IP_ADDR + "/listMemberEvent?"+parameters);
                connection = (HttpURLConnection) url.openConnection();
                BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = "";
                String resultStr = "";
                while ((line = bufferedReader.readLine()) != null) {
                    resultStr += line;
                }

                return resultStr;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            myEventList = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(json);
                for (int i=0; i<jsonArray.length(); i++) {
                    myEventList.add((String) jsonArray.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }



    private GoogleApiClient getGoogleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .build();
    }

    private void retrieveDeviceNode() {
        final GoogleApiClient client = getGoogleApiClient(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    nodeId = nodes.get(0).getId();
                }
                client.disconnect();
            }
        }).start();
    }

    private void sendEventMessage(final String eventMessage) {
        final GoogleApiClient client = getGoogleApiClient(this);
        if (nodeId != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(client, nodeId, eventMessage, null);
                    client.disconnect();
                }
            }).start();
        }
    }

}
