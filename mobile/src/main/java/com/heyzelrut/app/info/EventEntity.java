package com.heyzelrut.app.info;

public class EventEntity {
    private int eventNo;
    private String eventName;
    private String eventContent;
    private String eventStdate;
    private String eventEddate;
    private int eventSale;
    private int eventStat;
    private String eventImgpath;
    private String eventGendate;
    private int goodsNo;
    private int eventTypeNo;
    private int eventCombo;
    private int goodsCombo;

    public EventEntity() {

    }

    public EventEntity(int eventNo, String eventName, String eventContent, String eventStdate, String eventEddate,
                       int eventSale, int eventStat, String eventImgpath, String eventGendate, int goodsNo, int eventTypeNo,
                       int eventCombo, int goodsCombo) {
        this.eventNo = eventNo;
        this.eventName = eventName;
        this.eventContent = eventContent;
        this.eventStdate = eventStdate;
        this.eventEddate = eventEddate;
        this.eventSale = eventSale;
        this.eventStat = eventStat;
        this.eventImgpath = eventImgpath;
        this.eventGendate = eventGendate;
        this.goodsNo = goodsNo;
        this.eventTypeNo = eventTypeNo;
        this.eventCombo = eventCombo;
        this.goodsCombo = goodsCombo;
    }

    public int getEventCombo() {
        return eventCombo;
    }
    public void setEventCombo(int eventCombo) {
        this.eventCombo = eventCombo;
    }
    public int getGoodsCombo() {
        return goodsCombo;
    }
    public void setGoodsCombo(int goodsCombo) {
        this.goodsCombo = goodsCombo;
    }
    public int getEventNo() {
        return eventNo;
    }
    public void setEventNo(int eventNo) {
        this.eventNo = eventNo;
    }
    public String getEventName() {
        return eventName;
    }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
    public String getEventContent() {
        return eventContent;
    }
    public void setEventContent(String eventContent) {
        this.eventContent = eventContent;
    }
    public String getEventStdate() {
        return eventStdate;
    }
    public void setEventStdate(String eventStdate) {
        this.eventStdate = eventStdate;
    }
    public String getEventEddate() {
        return eventEddate;
    }
    public void setEventEddate(String eventEddate) {
        this.eventEddate = eventEddate;
    }
    public int getEventSale() {
        return eventSale;
    }
    public void setEventSale(int eventSale) {
        this.eventSale = eventSale;
    }
    public int getEventStat() {
        return eventStat;
    }
    public void setEventStat(int eventStat) {
        this.eventStat = eventStat;
    }
    public String getEventImgpath() {
        return eventImgpath;
    }
    public void setEventImgpath(String eventImgpath) {
        this.eventImgpath = eventImgpath;
    }
    public String getEventGendate() {
        return eventGendate;
    }
    public void setEventGendate(String eventGendate) {
        this.eventGendate = eventGendate;
    }
    public int getGoodsNo() {
        return goodsNo;
    }
    public void setGoodsNo(int goodsNo) {
        this.goodsNo = goodsNo;
    }
    public int getEventTypeNo() {
        return eventTypeNo;
    }
    public void setEventTypeNo(int eventTypeNo) {
        this.eventTypeNo = eventTypeNo;
    }
}