package com.heyzelrut.app.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.heyzelrut.app.heyzelrut.R;
import com.heyzelrut.app.info.EventEntity;
import com.heyzelrut.app.info.StaticInfo;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by user on 2015-08-25.
 */
public class EventAdapter extends BaseAdapter {
    public Context context;
    ArrayList<EventEntity> eventList;
    public LayoutInflater inflater;

    public EventAdapter(Context context, ArrayList<EventEntity> eventList){
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.eventList = eventList;
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Object getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int row = 0;
        if(convertView == null){
            if(eventList.get(position).getEventImgpath().equals("null")){
                row = R.layout.event_list_no_image;
            }else{
                row = R.layout.event_list_image;
            }
        }
        if(row != 0) {
            convertView = inflater.inflate(row, parent, false);
            if (eventList.get(position).getEventImgpath().equals("null")) {
                TextView eName = (TextView) convertView.findViewById(R.id.eventName2);
                eName.setText(eventList.get(position).getEventName());
                /*TextView eGoods = (TextView) convertView.findViewById(R.id.eventGoods2);
                eGoods.setText(String.valueOf(eventList.get(position).getGoodsNo()));
                TextView eStart = (TextView) convertView.findViewById(R.id.eventStart2);
                eStart.setText(eventList.get(position).getEventStdate());
                TextView eEnd = (TextView) convertView.findViewById(R.id.eventEnd2);
                eEnd.setText(eventList.get(position).getEventEddate());*/
            } else {
                ImageView eImg = (ImageView) convertView.findViewById(R.id.eventImg);
                String path = StaticInfo.IP_ADDR+"/eventImgs/"+eventList.get(position).getEventImgpath();
                try {
                    InputStream is = new URL(path).openStream();
                    Bitmap bit = BitmapFactory.decodeStream(is);
                    eImg.setImageBitmap(bit);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                TextView eName = (TextView) convertView.findViewById(R.id.eventName);
                eName.setText(eventList.get(position).getEventName());
                /*TextView eGoods = (TextView) convertView.findViewById(R.id.eventGoods);
                eGoods.setText(String.valueOf(eventList.get(position).getGoodsNo()));
                TextView eStart = (TextView) convertView.findViewById(R.id.eventStart);
                eStart.setText(eventList.get(position).getEventStdate());
                TextView eEnd = (TextView) convertView.findViewById(R.id.eventEnd);
                eEnd.setText(eventList.get(position).getEventEddate());*/
            }
        }
        return convertView;
    }
}
